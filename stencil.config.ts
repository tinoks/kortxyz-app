import { Config } from '@stencil/core';
import builtins from 'rollup-plugin-node-builtins';

export const config: Config = {
  enableCache: true,
  globalStyle: 'src/global/app.css',
  nodeResolve: {
    preferBuiltins: false,
    browser: true
  },
  plugins: [
    builtins()
  ]
};
