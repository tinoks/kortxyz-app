import { Component, Prop } from '@stencil/core';

@Component({
  tag: 'kortxyz-accordion',
  styleUrl: 'kortxyz-accordion.css'
})

export class KortxyzAccordion {
  @Prop() label: string = "Workspace";
  @Prop() active: boolean = false;

  componentWillLoad() {
  
  }

  render() {
    return ([
        <input class="accordion__input" id={this.label} type="checkbox" name="tabs" checked={this.active}></input>,
        <label class="accordion__label" htmlFor={this.label}>{this.label}</label>,
        <accordion_content class="accordion_content" id={this.label}>
            <slot />
        </accordion_content>
    ]);
  }
}
