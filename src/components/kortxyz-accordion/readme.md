# kortxyz-accordion



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type      |
| -------- | --------- | ----------- | --------- |
| `active` | `active`  |             | `boolean` |
| `label`  | `label`   |             | `string`  |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
