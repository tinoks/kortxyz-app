import { Component, Prop } from '@stencil/core';

@Component({
  tag: 'kortxyz-addfiles',
  styleUrl: 'kortxyz-addfiles.css'
})

export class KortxyzAddfiles {
  @Prop({ mutable: true }) results: any = [
    { name: "http://jordbrugsanalyser.dk/geoserver/wms?request=getCapabilities", type: "url" },
    { name: "http://jordbrugsanalyser.dk/geoserver/wfs?request=getCapabilities", type: "url" }
  ];

  removeDialog(e: any) {
    if (!e.path.filter(e => e.nodeName == "KORTXYZ-ADDFILES").length) {
      const elem = document.querySelector("KORTXYZ-ADDFILES");
      elem.parentNode.removeChild(elem);
    }
  }

  componentDidLoad() {
    document.body.addEventListener("click", this.removeDialog, true)
    const searchbar: HTMLInputElement = document.querySelector(".addfiles__searchfield");
    searchbar.onkeydown = (e) => {
      if (e.key == "Enter") {
        e.preventDefault();

        const url = searchbar.textContent;
        if (url.toLowerCase().includes("wms") || url.toLowerCase().includes("wfs")) {
          this.fetchOGC(url)
        }
        else {
          this.fetchESRI(url)
        }
      }
    };
  }

  componentDidUnload() {
    document.body.removeEventListener("click", this.removeDialog, true)
  }

  //http://jordbrugsanalyser.dk/geoserver/wms?request=getCapabilities
  fetchOGC = (url) => {

    fetch('https://kort.xyz/proxy/'+url)
      .then(res => res.arrayBuffer())
      .then(ab => {
        const dataView = new DataView(ab);
        const decoder = new TextDecoder("ISO8859-1");
        return decoder.decode(dataView);
      })
      .then(str => (new DOMParser()).parseFromString(str, "text/xml"))
      .then((xml) => {
       window["xml"] = xml;

        if (xml.firstElementChild.tagName.toUpperCase().includes("WMS") || xml.firstElementChild.tagName.toUpperCase().includes("WMT_MS")) {
          console.log("WMS")
          this.results = Array.from(xml.getElementsByTagName("Layer")[0].getElementsByTagName("Layer"))
            .map(e => {
              return {
                "name": e.childNodes[1].textContent,
                "type": "wms",
                "url": url
              }
            })
        }
        else if (xml.firstElementChild.tagName.toUpperCase().includes("WFS")) {
          this.results = Array.from(xml.getElementsByTagName("FeatureTypeList")[0].getElementsByTagName("FeatureType"))
            .map(e => {
              return {
                "name": e.childNodes[1].textContent,
                "type": "wfs",
                "url": url
              }
            })
        }
      })
    }


  // https://mapprod.sdfe.dk/app/rest/services/MDS?f=pjson
  fetchESRI = (url) => {
    fetch(url)
      .then(res => res.json())
      .then((json) => {
        console.log(json)
        /*json.services.forEach((e)=> {
                let layer = e.getElementsByTagName("Title")[0].innerHTML;
                let div = document.createElement("div")
                    div.className = "result"
                    div.innerText = layer
                wrapper.appendChild(div)
                        //console.log(url.split("?")[0],layer,undefined,false);
            });*/
      })
  }

  addWS(event: any) {
    const layer = event.target.textContent;
    const result = this.results.find(e => e.name == layer)

    if (result.type == "url") {
      this.fetchOGC(result.name)
    }
    else if (result.type == "wms") {
      const map = document.querySelector("kortxyz-map")
      map.addWMS(result.url.split("?")[0], result.name.split(":")[1], undefined, true)
    }
    else if (result.type == "wfs") {
      const map = document.querySelector("kortxyz-map")
      map.addWFS(result.url.split("?")[0], result.name, undefined, true)
    }
  }

  removeButtons(){
    const buttons = document.querySelectorAll(".addfiles__files")
          buttons.forEach(e=>e.remove());
  }

  newFile= () => {
    const elem = document.querySelector("KORTXYZ-ADDFILES");
    elem.parentNode.removeChild(elem);

    const div = document.createElement("div")
          div.contentEditable = "true";
          div.style.padding = "5px";
          div.style.background = "white";
          div.style.margin = "5px";

    const layerlist = document.querySelector("kortxyz-layerlist")
          layerlist.insertBefore(div,layerlist.childNodes[1]);
          
          div.focus();

          div.addEventListener("keydown",(e)=>{
            if(e.key == "Enter"){
              const emptyGeojson = {"type": "FeatureCollection","features": []};
              const mapdiv = document.querySelector("kortxyz-map");
              console.log(div.innerText)
              mapdiv.addLayer({
                'id': div.innerText,
                'type': 'fill',
                'source': {
                  'type': 'geojson',
                  'data': emptyGeojson
                },
                'layout': {},
                'paint': {}
              })

              div.remove();
            }
          })
    
  

/*

    mapdiv.addLayer({
            'id': 'empty',
            'type': 'fill',
            'source': {
              'type': 'geojson',
              'data': emptyGeojson
            },
            'layout': {
              'visibility': 'visible'
            },
            'paint': {
              'fill-color': 'skyblue',
              'fill-outline-color': 'white',
              'fill-opacity': 0.9
            }
          })
          */
  }

  openFile(){
    const input:HTMLInputElement = document.querySelector("#fileUpload");
          input.click();
  }

  render() {
    return ([
      <div class="addfiles__searchbar" >
        <div class="addfiles__searchfield" onClick={this.removeButtons} contentEditable></div>
        <div class="addfiles__files" onClick={this.newFile}>
        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24"><path d="M19 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-2 10h-4v4h-2v-4H7v-2h4V7h2v4h4v2z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
        </div>
        <div class="addfiles__files" onClick={this.openFile}>
        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 4H5c-1.11 0-2 .9-2 2v12c0 1.1.89 2 2 2h4v-2H5V8h14v10h-4v2h4c1.1 0 2-.9 2-2V6c0-1.1-.89-2-2-2zm-7 6l-4 4h3v6h2v-6h3l-4-4z"/></svg>
        </div>
      </div>,
      <div class="addfiles__results">
        {this.results.map((result) =>
          <div onClick={(event: MouseEvent) => this.addWS(event)} >{result.name}</div>
        )}
      </div>


    ]);
  }
}
