import { Component } from '@stencil/core';

@Component({
  tag: 'kortxyz-sidebar',
  styleUrl: 'kortxyz-sidebar.css'
})

export class KortxyzSidebar {
  render() {
    return ([
      <kortxyz-sideitem name="layerlist" icon="layers" >
        <kortxyz-layerlist id="layerlist"></kortxyz-layerlist>
      </kortxyz-sideitem>,
      <kortxyz-sideitem name="search" icon="search">
        <kortxyz-search></kortxyz-search>
      </kortxyz-sideitem>,
      <kortxyz-sideitem name="draw" icon="edit" small={true} >
        <kortxyz-draw></kortxyz-draw>
      </kortxyz-sideitem>,
      <kortxyz-sideitem name="analysis" icon="analysis" >

      </kortxyz-sideitem>,
      <kortxyz-sideitem name="apps" icon="apps">

      </kortxyz-sideitem>,
      <kortxyz-sideitem name="share" icon="share" >

      </kortxyz-sideitem>,
      <spacer></spacer>,
      <kortxyz-sideitem name="help" icon="help" small={true} >

      </kortxyz-sideitem>,
      <kortxyz-sideitem name="settings" icon="settings" small={true} >

      </kortxyz-sideitem>
    ])
  }
}
