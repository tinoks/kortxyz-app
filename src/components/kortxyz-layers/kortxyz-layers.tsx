import { Component, Prop, Listen } from '@stencil/core';

@Component({
  tag: 'kortxyz-layers',
  styleUrl: 'kortxyz-layers.css'
})

export class kortxyzLayers {

  @Prop() name: string;

  @Prop() active: boolean = true;

  @Listen('contextmenu')
  handleKeyDown(e){
    e.preventDefault();
    const map: HTMLKortxyzMapElement = document.querySelector("kortxyz-map");
          map.fitLayer(e.target.htmlFor); 
  }

  changeVisibility(e) {
    const map: HTMLKortxyzMapElement = document.querySelector("kortxyz-map");
    const style = window["map"].getLayoutProperty(e.target.id,"visibility") == "none"? "visible":"none";
    map.setLayerStyle(e.target.id,"visibility",style,"layout")
  }

  openConfig(e){
    const layername = e.target.nodeName == "svg" ?e.target.dataset.name : e.target.parentNode.dataset.name;
    const configMenu = document.querySelector("kortxyz-config")
    //console.log(layername)
    if(configMenu.configlayers.indexOf(layername) == -1){
      configMenu.addConfiglayer(layername);
    }
    else{
      const input:HTMLInputElement = document.querySelector("#"+layername+"_config");
      input.checked = true    
    }
  }

  render() {
    return ([
      <input type="checkbox" id={this.name}  onChange={this.changeVisibility} checked={this.active} />,
      <label htmlFor={this.name}>{this.name}</label>,
      <div class="button" onClick={ (event:UIEvent) => {this.openConfig(event)} }>
        <svg data-name={this.name} xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 20 20"><path fill="none" d="M0 0h20v20H0V0z"/><path d="M15.95 10.78c.03-.25.05-.51.05-.78s-.02-.53-.06-.78l1.69-1.32c.15-.12.19-.34.1-.51l-1.6-2.77c-.1-.18-.31-.24-.49-.18l-1.99.8c-.42-.32-.86-.58-1.35-.78L12 2.34c-.03-.2-.2-.34-.4-.34H8.4c-.2 0-.36.14-.39.34l-.3 2.12c-.49.2-.94.47-1.35.78l-1.99-.8c-.18-.07-.39 0-.49.18l-1.6 2.77c-.1.18-.06.39.1.51l1.69 1.32c-.04.25-.07.52-.07.78s.02.53.06.78L2.37 12.1c-.15.12-.19.34-.1.51l1.6 2.77c.1.18.31.24.49.18l1.99-.8c.42.32.86.58 1.35.78l.3 2.12c.04.2.2.34.4.34h3.2c.2 0 .37-.14.39-.34l.3-2.12c.49-.2.94-.47 1.35-.78l1.99.8c.18.07.39 0 .49-.18l1.6-2.77c.1-.18.06-.39-.1-.51l-1.67-1.32zM10 13c-1.65 0-3-1.35-3-3s1.35-3 3-3 3 1.35 3 3-1.35 3-3 3z"/></svg>
      </div>
    ] )
  }
  
}
