# kortxyz-layers



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type      |
| -------- | --------- | ----------- | --------- |
| `active` | `active`  |             | `boolean` |
| `name`   | `name`    |             | `string`  |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
