import { Component,Prop} from '@stencil/core';

@Component({
  tag: 'kortxyz-backgrounds',
  styleUrl: 'kortxyz-backgrounds.css'
})

export class kortxyzBackgrounds {
  @Prop() backgrounds: any =  [
    {name:'Basic'    , url: 'mapbox://styles/mapbox/basic-v9'},
    {name:'Streets'  , url: 'mapbox://styles/mapbox/streets-v9'},
    {name:'Bright'   , url: 'mapbox://styles/mapbox/bright-v9'},
    {name:'Light'    , url: 'mapbox://styles/mapbox/light-v9'},
    {name:'Dark'     , url: 'mapbox://styles/mapbox/dark-v9'},
    {name:'Satellite', url: 'mapbox://styles/mapbox/satellite-v9'},
    {name:'Empty'    , url: 'mapbox://styles/mapbox/empty-v9'},

    {name:'Support'  , url: 'mapbox://styles/tinoks/cjo3yutli4u6l2snnw71aub6r'},
    {name:'Desert'   , url:'mapbox://styles/tinoks/cjkqy51z556k02rmngdau0yjk'},
    {name:'Northstar', url: 'mapbox://styles/tinoks/cj48vu92z291x2soro9yhpd1c'},
    {name:'Decimal'  , url: 'mapbox://styles/tinoks/cjibgapw20h092sp11g1erri7'},
    {name:'Odyssey'  , url: 'mapbox://styles/tinoks/cjippk9y801i62sntvcq51gsp'},
    {name:'Terminal' , url: 'mapbox://styles/tinoks/cjibg7dmi1ebg2ro4emrrldu9'},
    {name:'Minimo'   , url: 'mapbox://styles/tinoks/cjkw2vzwj25ft2rpif8nr0zq7'}
  ];

  removeDialog(e:any){
    if(!e.path.filter(e=>e.nodeName == "KORTXYZ-BACKGROUNDS").length){
      const elem = document.querySelector("kortxyz-backgrounds");
            elem.parentNode.removeChild(elem);
    }
  }

  componentDidLoad() {
    document.body.addEventListener("click",this.removeDialog, true)
	}

  componentDidUnload(){
    document.body.removeEventListener("click",this.removeDialog, true)
  }

  changeBackground(e){
    let icon:any = document.querySelector("item#background")
        icon.innerText = e.target.innerText;

    document.querySelector("kortxyz-map").setStyle(e.target.dataset.url)
    const elem = document.querySelector("kortxyz-backgrounds");
          elem.parentNode.removeChild(elem);
  }

  render() {
    return ([
			<div>
        {this.backgrounds.map((background) =>
            <div class="background" onClick={this.changeBackground.bind(this)} data-url={background.url}>{background.name}</div>
        )}
      </div>
    ]);
  }
}
