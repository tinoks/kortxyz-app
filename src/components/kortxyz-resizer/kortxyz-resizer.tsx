import { Component} from '@stencil/core';

@Component({
  tag: 'kortxyz-resizer',
  styleUrl: 'kortxyz-resizer.css'
})

export class kortxyzResizer {

  right = document.querySelector("kortxyz-config");

  dragstart = () => {
    console.log("start")
    document.body.addEventListener('mousemove', this.drag, true)
    document.body.addEventListener('mouseup', this.dragstop,true )  
  }

   dragstop = () => {
     console.log("stop")
     document.body.removeEventListener('mousemove', this.drag, true)
   // document.body.removeEventListener('mouseup', this.dragstop,true )  
  } 

  drag = (e)=>{
    console.log("move",e.movementX)

    this.right.style.flex = `0 0 ${this.right.clientWidth-e.movementX}px`;
    window["map"].resize();
  }

  render() {
    return ([
      <div 
        onMouseDown={this.dragstart}
        >
      </div>
    ]);
  }
}
