import { Component,Prop } from '@stencil/core';

@Component({
  tag: 'kortxyz-search',
  styleUrl: 'kortxyz-search.css'
})

export class KortxyzSearch {
  @Prop({ mutable: true }) results: any = [];

  search(e){
    if(e.key=="Enter"){
      fetch(`https://kort.xyz/proxy/http://api.geonames.org/searchJSON?inclBbox=true&maxRows=10&username=tinoks&q=${e.target.innerText}`)
        .then(res=> res.json())
        .then(json => {
          this.results = json.geonames.filter(e=>e.lng!=undefined)
        })
    }
  }
  
  gotoPlace(e){
    //console.log(e)
    const bbox = e.target.className == "search__result"? e.target["data-bbox"] : e.target.parentNode["data-bbox"];
    if (bbox.length>0){
      window["map"].fitBounds(bbox)
    }
    else{
      const coords = e.target.className == "search__result"? e.target["data-pos"] : e.target.parentNode["data-pos"];
      document.querySelector("kortxyz-map").setCenter(coords)
    }

  }

  

  render() {
    return ([
      <div class="search__input" contentEditable onKeyUp={(e:UIEvent)=>this.search(e)}></div>,
      <div>
        {this.results.map((result) =>
          <div class="search__result" data-bbox={result.bbox ? [result.bbox.west,result.bbox.south,result.bbox.east,result.bbox.north]: []} data-pos={[result.lng,result.lat]} onClick={(event:UIEvent) => this.gotoPlace(event)}>
            <sub>{result.countryName}</sub><br/>
            <b>{result.toponymName}</b><br/>
            <i>{result.fcodeName}</i>
          </div>
        )}
      </div>
    ])
  }
}
