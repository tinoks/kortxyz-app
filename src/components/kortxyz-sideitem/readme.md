# kortxyz-sideitem



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type      |
| -------- | --------- | ----------- | --------- |
| `icon`   | `icon`    |             | `string`  |
| `name`   | `name`    |             | `string`  |
| `small`  | `small`   |             | `boolean` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
