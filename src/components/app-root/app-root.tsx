import { Component,Listen } from '@stencil/core';


@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {

  @Listen('layerAdded')
  layerAddedHandler(data) {
    const ws: any = document.querySelector("kortxyz-layerlist")
    let layers: HTMLKortxyzLayersElement = document.createElement("kortxyz-layers");
        layers.name = data.detail.id;
        ws.insertBefore(layers,ws.childNodes[1]);       
  }

  @Listen('mapMoved')
  mapMovedHandler(center) {
    const footer = document.querySelector("kortxyz-footer");
          footer.changeCoords([center.detail.lng,center.detail.lat]);
  }

  render() {
    return ([
        <app-row>
          <kortxyz-sidebar></kortxyz-sidebar>
          <kortxyz-map dropfile={true} accesstoken="pk.eyJ1IjoidGlub2tzIiwiYSI6ImNqM2p5d2hkbTAwM3UzMnBwbWF2NG96Z3IifQ._hWk-eEzh8sNjp3qA_cJuQ"></kortxyz-map>
          <kortxyz-config></kortxyz-config>
        </app-row>,
        <kortxyz-footer></kortxyz-footer>
    ]);
  }
}
