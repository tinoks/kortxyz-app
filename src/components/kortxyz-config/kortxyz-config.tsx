import { Component, Prop, Method } from '@stencil/core';

@Component({
  tag: 'kortxyz-config',
  styleUrl: 'kortxyz-config.css'
})

export class kortxyzConfig {
  @Prop({mutable:true}) configlayers: Array<string> = [];

  @Method()
  removeAll(){
    this.configlayers= [];
  }

  @Method()
  addConfiglayer(layername){
   // this.configlayers.push(layer);
    this.configlayers = [
      ...this.configlayers,
      layername
    ]
    setTimeout(() => {
      const input:HTMLInputElement = document.querySelector("#"+layername+"_config");
            input.checked = true    
    }, 60);

  }

  @Method()
  closeConfig(layername){
    if(typeof layername != "string"){
      layername.target.parentElement.previousElementSibling.checked = false;
      layername = layername.target.previousSibling.innerText;
    }


    const index = this.configlayers.indexOf(layername);
        this.configlayers.splice(index,1)
        this.configlayers = [
          ...this.configlayers
        ]
 
    
  }

  componentDidLoad(){
    console.log(this.configlayers)
  }

  componentDidUpdate(){
    //console.log("update")
    window["map"].resize();
  }


  dragstart = () => {
    document.body.addEventListener('mousemove', this.drag, true)
    document.body.addEventListener('mouseup', this.dragstop,true )  
  }

  dragstop = () => {
     document.body.removeEventListener('mousemove', this.drag, true)
  } 

  drag = (e)=>{
    if(e.clientX<40){this.dragstop()}
    let container:any = document.querySelector(".config__container")
    container.style.width = `${window.innerWidth-e.clientX}px`;
    window["map"].resize();
  }


  startWidth = {    width:'300px'  }
  render() {
    if (this.configlayers.length != 0) {
      return ( [
        <div class="config__container" style={this.startWidth}>
          <resizer  onMouseDown={this.dragstart} ></resizer>
          <ul class="config__tabs">
              {this.configlayers.map((configlayer) =>
              <li class="config__list">
                <input class="config__input" type="radio" name="config__tabs" id={configlayer+"_config"}></input>
                <label class="config__label" htmlFor={configlayer+"_config"}>
                  <div class="config__label--text">{configlayer}</div>
                  <div class="config__label--close" onClick={ (event:UIEvent) => {this.closeConfig(event)} }>X</div>
                </label>
                <div class="config__content">
                  <kortxyz-configlayer layer={configlayer}></kortxyz-configlayer>
                </div>
              </li>
              )}
          </ul>
        </div>

      ]

      
        )
    }
  }
}
