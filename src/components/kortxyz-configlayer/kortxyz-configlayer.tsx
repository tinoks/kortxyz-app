import { Component, Prop} from '@stencil/core';
import jamsession from '@mapbox/expression-jamsession';

@Component({
  tag: 'kortxyz-configlayer',
  styleUrl: 'kortxyz-configlayer.css'
})

export class kortxyzConfiglayer {
  @Prop() layer: any;

  layerJSON:any;
  paint:any;

  componentWillLoad() {
    console.log("componentWillLoad")
    this.layerJSON = document.querySelector("kortxyz-map").layers.filter(e=>e.id==this.layer)[0];
    console.log(this.layerJSON)
    this.paint = Object.entries({...this.spec[this.layerJSON.type] , ...this.layerJSON.paint})
	}

  componentWillUpdate(){
    console.log("componentWillUpdate")
    this.layerJSON = document.querySelector("kortxyz-map").layers.filter(e=>e.id==this.layer)[0];
    console.log(this.layerJSON)

    this.paint = Object.entries({...this.spec[this.layerJSON.type] , ...this.layerJSON.paint})
  }

  
  spec = { 
    "fill": {
      "fill-antialias": "",
      "fill-opacity": "",
      "fill-color": "",
      "fill-outline-color": "",
      "fill-translate": "",
      "fill-translate-anchor": "",
      "fill-pattern": ""
    },
    "circle": {
      "circle-radius": "",
      "circle-color": "",
      "circle-blur": "",
      "circle-opacity": "",
      "circle-translate": "",
      "circle-translate-anchor": "",
      "circle-pitch-scale": "",
      "circle-pitch-alignment": "",
      "circle-stroke-width": "",
      "circle-stroke-color": "",
      "circle-stroke-opacity": "",
    },
    "line":{
      "line-cap": "",
      "line-join": "",
      "line-miter-limit": "",
      "line-round-limit": "",
      "line-opacity": "",
      "line-color":"",
      "line-translate": "",
      "line-translate-anchor": "",
      "line-width": "",
      "line-gap-width": "",
      "line-offset": "",
      "line-blur": "",
      "line-dasharray": "",
      "line-pattern": "",
      "line-gradient": "",
    },
  };
  /*
    "circle": {
      "circle-radius": DataDrivenProperty<number>,
      "circle-color": DataDrivenProperty<Color>,
      "circle-blur": DataDrivenProperty<number>,
      "circle-opacity": DataDrivenProperty<number>,
      "circle-translate": DataConstantProperty<[number, number]>,
      "circle-translate-anchor": DataConstantProperty<"map" | "viewport">,
      "circle-pitch-scale": DataConstantProperty<"map" | "viewport">,
      "circle-pitch-alignment": DataConstantProperty<"map" | "viewport">,
      "circle-stroke-width": DataDrivenProperty<number>,
      "circle-stroke-color": DataDrivenProperty<Color>,
      "circle-stroke-opacity": DataDrivenProperty<number>,
    },
    "fill-extrusion":{
      "fill-extrusion-opacity": DataConstantProperty<number>,
      "fill-extrusion-color": DataDrivenProperty<Color>,
      "fill-extrusion-translate": DataConstantProperty<[number, number]>,
      "fill-extrusion-translate-anchor": DataConstantProperty<"map" | "viewport">,
      "fill-extrusion-pattern": CrossFadedDataDrivenProperty<string>,
      "fill-extrusion-height": DataDrivenProperty<number>,
      "fill-extrusion-base": DataDrivenProperty<number>,
      "fill-extrusion-vertical-gradient": DataConstantProperty<boolean>,
    },*/
   /*"fill": {
      "fill-antialias": "DataConstantProperty<boolean>",
      "fill-opacity": DataDrivenProperty<number>,
      "fill-color": DataDrivenProperty<Color>,
      "fill-outline-color": DataDrivenProperty<Color>,
      "fill-translate": DataConstantProperty<[number, number]>,
      "fill-translate-anchor": DataConstantProperty<"map" | "viewport">,
      "fill-pattern": CrossFadedDataDrivenProperty<string>,
    }/,
    "heatmap":{
      "heatmap-radius": DataDrivenProperty<number>,
      "heatmap-weight": DataDrivenProperty<number>,
      "heatmap-intensity": DataConstantProperty<number>,
      "heatmap-color": ColorRampProperty,
      "heatmap-opacity": DataConstantProperty<number>,
    },
    "line":{
      "line-cap": DataConstantProperty<"butt" | "round" | "square">,
      "line-join": DataDrivenProperty<"bevel" | "round" | "miter">,
      "line-miter-limit": DataConstantProperty<number>,
      "line-round-limit": DataConstantProperty<number>,

      "line-opacity": DataDrivenProperty<number>,
      "line-color": DataDrivenProperty<Color>,
      "line-translate": DataConstantProperty<[number, number]>,
      "line-translate-anchor": DataConstantProperty<"map" | "viewport">,
      "line-width": DataDrivenProperty<number>,
      "line-gap-width": DataDrivenProperty<number>,
      "line-offset": DataDrivenProperty<number>,
      "line-blur": DataDrivenProperty<number>,
      "line-dasharray": CrossFadedProperty<Array<number>>,
      "line-pattern": CrossFadedDataDrivenProperty<string>,
      "line-gradient": ColorRampProperty,
    },
    "symbol":{
      "symbol-placement": DataConstantProperty<"point" | "line" | "line-center">,
      "symbol-spacing": DataConstantProperty<number>,
      "symbol-avoid-edges": DataConstantProperty<boolean>,
      "symbol-z-order": DataConstantProperty<"viewport-y" | "source">,
      "icon-allow-overlap": DataConstantProperty<boolean>,
      "icon-ignore-placement": DataConstantProperty<boolean>,
      "icon-optional": DataConstantProperty<boolean>,
      "icon-rotation-alignment": DataConstantProperty<"map" | "viewport" | "auto">,
      "icon-size": DataDrivenProperty<number>,
      "icon-text-fit": DataConstantProperty<"none" | "width" | "height" | "both">,
      "icon-text-fit-padding": DataConstantProperty<[number, number, number, number]>,
      "icon-image": DataDrivenProperty<string>,
      "icon-rotate": DataDrivenProperty<number>,
      "icon-padding": DataConstantProperty<number>,
      "icon-keep-upright": DataConstantProperty<boolean>,
      "icon-offset": DataDrivenProperty<[number, number]>,
      "icon-anchor": DataDrivenProperty<"center" | "left" | "right" | "top" | "bottom" | "top-left" | "top-right" | "bottom-left" | "bottom-right">,
      "icon-pitch-alignment": DataConstantProperty<"map" | "viewport" | "auto">,
      "text-pitch-alignment": DataConstantProperty<"map" | "viewport" | "auto">,
      "text-rotation-alignment": DataConstantProperty<"map" | "viewport" | "auto">,
      "text-field": DataDrivenProperty<Formatted>,
      "text-font": DataDrivenProperty<Array<string>>,
      "text-size": DataDrivenProperty<number>,
      "text-max-width": DataDrivenProperty<number>,
      "text-line-height": DataConstantProperty<number>,
      "text-letter-spacing": DataDrivenProperty<number>,
      "text-justify": DataDrivenProperty<"left" | "center" | "right">,
      "text-anchor": DataDrivenProperty<"center" | "left" | "right" | "top" | "bottom" | "top-left" | "top-right" | "bottom-left" | "bottom-right">,
      "text-max-angle": DataConstantProperty<number>,
      "text-rotate": DataDrivenProperty<number>,
      "text-padding": DataConstantProperty<number>,
      "text-keep-upright": DataConstantProperty<boolean>,
      "text-transform": DataDrivenProperty<"none" | "uppercase" | "lowercase">,
      "text-offset": DataDrivenProperty<[number, number]>,
      "text-allow-overlap": DataConstantProperty<boolean>,
      "text-ignore-placement": DataConstantProperty<boolean>,
      "text-optional": DataConstantProperty<boolean>,
      
      "icon-opacity": DataDrivenProperty<number>,
      "icon-color": DataDrivenProperty<Color>,
      "icon-halo-color": DataDrivenProperty<Color>,
      "icon-halo-width": DataDrivenProperty<number>,
      "icon-halo-blur": DataDrivenProperty<number>,
      "icon-translate": DataConstantProperty<[number, number]>,
      "icon-translate-anchor": DataConstantProperty<"map" | "viewport">,
      "text-opacity": DataDrivenProperty<number>,
      "text-color": DataDrivenProperty<Color>,
      "text-halo-color": DataDrivenProperty<Color>,
      "text-halo-width": DataDrivenProperty<number>,
      "text-halo-blur": DataDrivenProperty<number>,
      "text-translate": DataConstantProperty<[number, number]>,
      "text-translate-anchor": DataConstantProperty<"map" | "viewport">,
    }*/
  
  changePaintproperty(property){
    let style;
    try {
      style = jamsession.formulaToExpression(property.target.value)
      document.querySelector("kortxyz-map").setLayerStyle(this.layerJSON.id, property.target.dataset.for,style,"paint");
    } catch (error) {
      console.error("fejl",error.description)
    }
  }

  closeDialog(){
    document.querySelector("kortxyz-configlayer").remove();
    const configMenu = document.querySelector("kortxyz-config");
    configMenu.style.flex = "0 0 0";
    window["map"].resize();
  }

  render() {
    return ([
/*      <div>name</div>,
      <textarea >{this.layer.id}</textarea>,
      <div>layout</div>,
      <textarea>
       {JSON.stringify(this.layer.layout)}
      </textarea>,
      <div>paint</div>,*/
      <kortxyz-accordion label={this.layerJSON.id+"_paint"}>
        {this.paint.map((property) => 
          <div>
              <div>{property[0]}</div>
              <textarea data-for={property[0]} onKeyUp={this.changePaintproperty.bind(this)}>
                { typeof property[1]=="object"? jamsession.expressionToFormula(property[1]): typeof property[1]=="string" && property[1].length>0 ? '"'+property[1]+'"' :property[1] }
              </textarea>
            </div>
          )
        }
      </kortxyz-accordion>
    ]);
  }
}
