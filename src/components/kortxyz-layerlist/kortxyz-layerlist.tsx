import { Component,Method } from '@stencil/core';
import { default as Sortable } from 'sortablejs';


@Component({
  tag: 'kortxyz-layerlist',
  styleUrl: 'kortxyz-layerlist.css'
})

export class kortxyzLayerlist { 
  componentDidLoad() {

    const icon = document.createElement("i");
          icon.className = "layerlist__addWS";
          icon.title = "Add layers";
          icon.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24">
                              <path d="M0 0h24v24H0z" fill="none"/>
                              <path d="M4 6H2v14c0 1.1.9 2 2 2h14v-2H4V6zm16-4H8c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-1 9h-4v4h-2v-4H9V9h4V5h2v4h4v2z"/>
                            </svg>`;
          icon.onclick = ()=>{
                this.openModal();
          }

    const header = document.querySelector("toolbox");
          header.appendChild(icon);

    window["Sortable"] = Sortable;
    
    Sortable.create(document.querySelector("kortxyz-layerlist"), {
            animation:200,
            onEnd: function (/**Event*/evt) {
                window["map"].moveLayer(evt.item.name,evt.item.previousSibling.name)
                },
    });
  }
  /*
  *
  */
  @Method()
  openModal(){
    const addMenu: HTMLKortxyzAddfilesElement  = document.createElement("kortxyz-addfiles");
          document.body.appendChild(addMenu)
  }

  handleFile(e){
    const filesArray = Array.from(e.target.files).reduce((groups, item:any) => {
      var val = item.name.split(".")[0];
        groups[val] = groups[val] || [];
        groups[val].push(item);
      return groups;
    }, {});
         
    for(const files in filesArray){
      const file = filesArray[files][0];
      let type = file.name.split(".").pop().toLowerCase();
          type = type == "json" ? "geojson" : type;
      import(`./${type}.js`).then(m => { m.load(file); });
    }
  }

  render() {
    return ([
        <input type="file" id="fileUpload" onChange={this.handleFile} multiple accept=".fit,.geojson,.gpx,.json,.kml,.zip"></input>,
    ]);
  }
}
