# kortxyz-footer



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description | Type     |
| ---------- | ----------- | ----------- | -------- |
| `gpsState` | `gps-state` |             | `string` |


## Methods

| Method         | Description                               |
| -------------- | ----------------------------------------- |
| `changeCoords` | Change the MGRS coordinates in the bottom |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
