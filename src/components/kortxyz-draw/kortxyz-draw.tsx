import { Component, Prop, State} from '@stencil/core';
import { default as MapboxDraw} from '@mapbox/mapbox-gl-draw';
import { default as togpx} from 'togpx';
import { default as download} from 'downloadjs';

 
@Component({
  tag: 'kortxyz-draw',
  styleUrl: 'kortxyz-draw.css'
})

export class kortxyzDraw {


  @Prop({mutable:true}) drawLayers = ["Click to chose layer"];
  @State() activeLayer = "";
  

  draw = new MapboxDraw({displayControlsDefault: true,controls:[]});
  geojson:any = {};

  componentDidLoad() {

    
	}

  startDraw(e){
    if(e.target.value != "Click to chose layer"){

      window["map"].addControl(this.draw)
      window["map"].on('draw.create',  (e) => {
        this.geojson.features = [
          ...this.geojson.features,
          ...e.features
        ]
      });
      window["map"].on('draw.delete',  (e) => {
       console.log(e)
      });

      this.activeLayer = e.target.value;
      
      window["map"].removeLayer(this.activeLayer)
      this.geojson = window["map"].getSource(this.activeLayer).serialize().data
      this.draw.set(this.geojson)
     // draw.changeMode('simple_select')
    }

  }

  populateList=()=>{
    const geojsonLayers = document.querySelector("kortxyz-map").layers.filter(e=>e.source.type == "geojson" || typeof e.source == "string").map(e=>e.id);
    this.drawLayers =  ["Click to chose layer"];
    this.drawLayers = [
      ...this.drawLayers,
      ...geojsonLayers
    ];
  }

  finishDraw = () => {
    const map = window["map"];
    map.getSource(this.activeLayer).setData(this.draw.getAll())
    this.draw.set({"type": "FeatureCollection","features": []})

    let tempLayer = document.querySelector("kortxyz-map").layers.filter(e=>e.id==this.activeLayer)[0]
    tempLayer.source = this.activeLayer;
    map.addLayer(tempLayer);

    this.activeLayer = "";
    
    const dropdown:any = document.querySelector(".draw__layer");
        dropdown.selectedIndex = 0;
    map.removeControl(this.draw)
    map.boxZoom.enable()

  }

  exportGPX = () => {
    console.log(this.draw.getAll())
    download(togpx(this.draw.getAll()),'test.gpx',"text/plain")
  }

  render() {
    return ([
      <select class="draw__layer" onMouseEnter={this.populateList} onChange={(event: UIEvent) => this.startDraw(event)}>
      {this.drawLayers.map((drawLayer) =>
          <option value={drawLayer}>{drawLayer} </option>
        )}
      </select>,
      <div>
       {this.activeLayer ?
       <div>
        <div class="draw__layer" onClick={ () => this.draw.changeMode("draw_point")}>Draw Point</div>
        <div class="draw__layer" onClick={ () => this.draw.changeMode("draw_line_string")}>Draw Linestring</div>
        <div class="draw__layer" onClick={ () => this.draw.changeMode("draw_polygon")}>Draw Polygon</div>

        <div class="draw__layer">SPACER</div>
        <div class="draw__layer" onClick={ () =>  this.finishDraw() }>Finish drawing</div>
        <div class="draw__layer" onClick={ () =>  this.exportGPX() }>Export to GPX</div>
       </div>
       : null
        }
      </div>
      
    ]);
  }
}
