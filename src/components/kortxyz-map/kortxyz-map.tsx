import { Component, Prop, Method, Event, EventEmitter } from '@stencil/core';
import { default as mapboxgl } from 'mapbox-gl';
import { default as bbox } from '@turf/bbox';
import { default as bearing } from '@turf/bearing';
import { default as turf } from '@turf/helpers';

@Component({
  tag: 'kortxyz-map',
  styleUrl: 'kortxyz-map.css'
})

export class KortxyzMap {
  @Prop() accesstoken: string;
  @Prop({ mutable: true }) mapstyle: any = 'https://free.tilehosting.com/styles/basic/style.json?key=qmiUgsEporZLdttoqWLA';
  @Prop() dropfile: boolean = false;
  @Prop({ mutable: true }) layers: any = [];

  @Event() layerAdded: EventEmitter;
  @Event() mapMoved: EventEmitter;


  map: mapboxgl.Map;
  mapboxLayer: {
    'id': string,
    'type': string,
    'source': object,
    'layout': object,
    'paint': object
  };

  componentDidLoad() {
    mapboxgl.accessToken = this.accesstoken;
    this.map = new mapboxgl.Map({
      container: 'map',
      attributionControl: false,
      style: this.mapstyle,
      center: [0, 20],
      zoom: 2
    });
    this.map.on('moveend', () => {
      let center = this.map.getCenter();
      this.mapMoved.emit(center);
    })
    this.mapMoved.emit(this.map.getCenter());

    window["map"] = this.map;
    document.querySelector(".mapboxgl-control-container").remove();

    if (this.dropfile) {
      this.activateDrop()
    }
    this.touchPitch()
    if(window.location.hostname == "localhost"){
      this.map.on('load', ()=> {
        const testData = { "type": "FeatureCollection", "features": [ { "type": "Feature", "properties": {"id":1,"text":"test"}, "geometry": { "type": "Polygon", "coordinates": [ [ [ 6.240234374999999, 62.103882522897855 ], [ -44.033203125, 41.376808565702355 ], [ 27.0703125, 24.686952411999155 ], [ 6.240234374999999, 62.103882522897855 ] ] ] } } ] };
        document.querySelector("kortxyz-map").addGeojson('testdata',testData,true);
      });
    }

  }

  activateDrop() {
    let mapdiv = document.getElementById("map");

    mapdiv.addEventListener("dragleave", () => {
      mapdiv.style.opacity = '1';
    });

    mapdiv.addEventListener("dragover", (e) => {
      e.preventDefault();
      mapdiv.style.opacity = '0.5';
    });

    mapdiv.addEventListener("drop", (e) => {
      e.preventDefault();

      mapdiv.style.opacity = '1';

      const filesArray = Array.from(e.dataTransfer.files).reduce((groups, item) => {
        var val = item.name.split(".")[0];
        groups[val] = groups[val] || [];
        groups[val].push(item);
        return groups;
      }, {});
      
      for(const files in filesArray){
        if(filesArray[files].length>1){
          import(`../../modules/shp.js`).then(m => { m.load(filesArray[files]); })
        }
        else{
          const file = filesArray[files][0];
          const type = file.name.split(".").pop().toLowerCase();
          if(type == "geojson" || type == "json"){
            import("../../modules/geojson.js").then(m => { m.load(file); });
          }else if (type == "fit"){
            import("../../modules/fit.js").then(m => { m.load(file); });
          }else if (type == "zip"){
            import("../../modules/zip.js").then(m => { m.load(file); });
          }else if (type == "kml"){
            import("../../modules/kml.js").then(m => { m.load(file); });
          }else if (type == "gpx"){
            import("../../modules/gpx.js").then(m => { m.load(file); });
          }else{
            console.info(`filetype ${type} not supportet`)
          }
        }
      }


    });
  }

  touchPitch() {
    let yDown = null;

    let handleTouchStart = (evt) => {
      if (evt.touches.length == 3) {
        yDown = evt.touches[0].clientY;
      }
    };

    let handleTouchMove = (evt) => {
      if (!yDown || evt.touches.length != 3) return;
      const yUp = evt.touches[0].clientY;
      const yDiff = yDown - yUp;
      this.map.setPitch(this.map.getPitch() + yDiff / 10);
    };

    document.addEventListener('touchstart', handleTouchStart, false);
    document.addEventListener('touchmove', handleTouchMove, false);

  }


  @Method()
  setCenter(newCenter) {
    if (this.map.getPitch()) {
      const oldCenter = this.map.getCenter().toArray()
      const newBearing = bearing(turf.point(oldCenter), turf.point(newCenter));
      this.map.easeTo({
        bearing: newBearing,
        center: newCenter
      })

    }
    else {
      this.map.easeTo({ "center": newCenter })
    }
  }

  @Method()
  changeVisibility(clickedLayer) {
    let visibility = this.map.getLayoutProperty(clickedLayer, 'visibility');

    if (visibility === 'visible') {
      this.map.setLayoutProperty(clickedLayer, 'visibility', 'none');
    } else {
      this.map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
    }
  }

  @Method()
  fitLayer(layer: string) {
    this.map.fitBounds(
      bbox(this.map.getSource(layer).serialize().data)
    );
  }



  @Method()
  addLayer(data: {
    'id': string,
    'type': string,
    'source': object,
    'layout': object,
    'paint': object
  }) {
    this.layers.push(data)
    this.map.addLayer(data);
    this.layerAdded.emit(data);
    if (data.type != 'raster') {
      this.map.on('click', data.id, (e) => {
        new mapboxgl.Popup()
          .setLngLat(e.lngLat)
          .setHTML(Object.entries(e.features[0].properties).map(e => `<B>${e[0]}</B> ${e[1]}`).join("<BR>"))
          .addTo(this.map);
      });

      // Change the cursor to a pointer when the mouse is over the states layer.
      this.map.on('mouseenter', data.id, () => {
        this.map.getCanvas().style.cursor = 'pointer';
      });

      // Change it back to a pointer when it leaves.
      this.map.on('mouseleave', data.id, () => {
        this.map.getCanvas().style.cursor = '';
      });
    }
  }


  @Method()
  setStyle(url) {
    this.mapstyle = url;
    this.map.setStyle(url)
    this.map.once('style.load', () => {
      this.layers.forEach(data => {
        this.map.addLayer(data);
      })
    })
  }

  @Method()
  setLayerStyle(layer, property, style, type) {
    let errorMessage = "";
    this.map.on('error', e => {
      errorMessage = e.error.message
      console.error(e.error.message)
    })
    if (type == "paint") {
      this.map.setPaintProperty(layer, property, style)
      if(!errorMessage) this.layers.find(e=> e.id==layer).paint[property] = style;
    }
    if (type == "layout") {
      this.map.setLayoutProperty(layer, property, style)
      if(!errorMessage) this.layers.find(e=> e.id==layer).layout[property] = style;
    }
  }

  @Method()
  addGeojson(title, data, visibility) {
    visibility = typeof visibility === "undefined" ? true:visibility;

    title = title.lastIndexOf(".") == -1 ? title : title.substr(0, title.lastIndexOf("."));
    const typeTranslate = {
      "Point": "circle",
      "MultiPoint": "circle",
      "MultiLineString": "line",
      "LineString": "line",
      "Polygon": "fill",
      "MultiPolygon": "fill"
    };

    const styles = {
      "fill": {
        'fill-color': '#'+('00000'+(Math.random()*(1<<24)|0).toString(16)).slice(-6),
        'fill-opacity': 0.8
      },
      "line": {
        'line-color': '#'+('00000'+(Math.random()*(1<<24)|0).toString(16)).slice(-6),
        'line-opacity': 0.8
      },
      "circle": {
        'circle-color': '#'+('00000'+(Math.random()*(1<<24)|0).toString(16)).slice(-6),
        'circle-opacity': 0.8
      }
    }

    const type = typeTranslate[data.features[0].geometry.type];
    this.map.fitBounds(bbox(data));

    this.addLayer({
      'id': title,
      'type': type,
      'source': {
        'type': 'geojson',
        'data': data
      },
      'layout': {
        'visibility': visibility ? 'visible': 'none'
      },
      'paint': styles[type]
    })
  }

  @Method()
  addWMS(server, layer, ekstra, visibility) {
    ekstra = ekstra === undefined ? "" : ekstra;

    this.addLayer({
      'id': layer,
      'type': 'raster',
      'source': {
        'type': 'raster',
        'tiles': [
          `${server}?bbox={bbox-epsg-3857}&transparent=TRUE&format=image/png&service=WMS&version=1.3.0&request=GetMap&crs=EPSG:3857&width=512&height=512&layers=${layer}${ekstra}`
        ],
        'tileSize': 512
      },
      'paint': {},
      'layout': {
        'visibility': visibility ? 'visible' : 'none'
      }
    })
  }

  @Method()
  addWFS(server, layer, ekstra, visibility) {
    ekstra = ekstra === undefined ? "" : "&" + ekstra.join("&");

    fetch(`${server}?service=WFS&request=GetFeature&typeName=${layer}&outputFormat=JSON&srsName=EPSG:4326&count=1000`)
        .then(res => {
          if(res.headers.get("content-type").includes("ISO-8859-1")){
            res.arrayBuffer()
              .then(ab => {
                const dataView = new DataView(ab);
                const decoder = new TextDecoder("ISO8859-1");
                return decoder.decode(dataView);
              })
              .then(str => JSON.parse(str))
              .then((data) => {
                if(data.crs !== undefined){delete data.crs}; 
                this.addGeojson(layer, data, visibility)
              })
          }
          else{
            res.json()
              .then((data)=> {
                if(typeof data.crs != "undefined"){delete data.crs};               
                this.addGeojson(layer, data, visibility)
              } )
          }
        })

  }

  @Method()
  addWMTS(server, layer, visibility) {

    this.addLayer({
      'id': layer,
      'type': 'raster',
      'source': {
        'type': 'raster',
        'tiles': [
          `${server}?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&LAYER=${layer}&STYLE=&TILEMATRIX=EPSG:900913:{z}&TILEMATRIXSET=EPSG:900913&FORMAT=image/png&TILECOL={x}&TILEROW={y}`
        ],
        "tileSize": 256
      },
      'paint': {},
      'layout': {
        'visibility': visibility ? 'none' : 'visible'
      }
    })
  }

  @Method()
  addTMS(server, layer, visibility) {

    this.addLayer({
      'id': layer,
      'type': 'raster',
      'source': {
        'type': 'raster',
        'tiles': [
          `${server}/{z}/{x}/{y}.png`
          //http://10.144.142.115/Kort100/11/1094/1418.png
        ],
        "tileSize": 256,
        "scheme": "tms"
      },
      'paint': {},
      'layout': {
        'visibility': visibility ? 'none' : 'visible'
      }
    })
  }

  @Method()
  addEsriTMS(server, group, layer, visibility) {
    this.addLayer({
      'id': layer,
      'type': 'raster',
      'source': {
        'type': 'raster',
        'tiles': [
          `${server}/rest/services/${group}/${layer}/MapServer/tile/{z}/{y}/{x}`
          //http://10.144.142.115/arcgis/rest/services/SeaCharts/Kort100/MapServer/tile/0/0/0
          // addEsriTMS('http://10.144.142.115/arcgis','SeaCharts','Kort100')
        ],
        "tileSize": 256
      },
      'paint': {},
      'layout': {
        'visibility': visibility ? 'none' : 'visible'
      }
    })
  }

  @Method()
  removeLayer(name: string) {
    this.map.removeLayer(name)
    this.map.removeSource(name)
  }

  render() {
    return (
      <div id='map'></div>
    );
  }
}