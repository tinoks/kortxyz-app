# kortxyz-map



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute     | Description | Type      |
| ------------- | ------------- | ----------- | --------- |
| `accesstoken` | `accesstoken` |             | `string`  |
| `dropfile`    | `dropfile`    |             | `boolean` |
| `mapstyle`    | --            |             | `any`     |


## Events

| Event        | Description |
| ------------ | ----------- |
| `layerAdded` |             |
| `mapMoved`   |             |


## Methods

| Method             | Description |
| ------------------ | ----------- |
| `addEsriTMS`       |             |
| `addGeojson`       |             |
| `addLayer`         |             |
| `addTMS`           |             |
| `addWMS`           |             |
| `addWMTS`          |             |
| `changeVisibility` |             |
| `fitLayer`         |             |
| `removeLayer`      |             |
| `setCenter`        |             |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
