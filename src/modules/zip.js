import {default as shpjs} from 'shpjs';

function readShp (file) {
  var reader = new FileReader();

      reader.onload = ((res) => {

        shpjs(res.target.result)
          .then(function(shape) {
            const div = document.querySelector("KORTxyz-map");
            div.addGeojson(file.name,shape)
          }).catch(function(error) {
            console.log(error);
          });
       
      });

      reader.readAsArrayBuffer(file);
}
  
export function load(file) {
  readShp(file);
}