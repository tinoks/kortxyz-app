function readGeojson (file) {
  var reader = new FileReader();

    	reader.onload = ((res) => {
        const map = document.querySelector("KORTxyz-map");
        map.addGeojson(file.name,JSON.parse(res.target.result))
   		});

    	reader.readAsText(file);
}
  
export function load(file) {
    readGeojson(file);
}