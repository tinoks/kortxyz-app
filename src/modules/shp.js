import {read as readShape} from 'shapefile';
import {default as proj4} from 'proj4';

const readUploadedFileAsText = (inputFile) => {
    const temporaryFileReader = new FileReader();
  
    return new Promise((resolve, reject) => {
      temporaryFileReader.onerror = () => {
        temporaryFileReader.abort();
        reject(new DOMException("Problem parsing input file."));
      };
  
      temporaryFileReader.onload = () => {
        resolve(temporaryFileReader.result);
      };
      temporaryFileReader.readAsText(inputFile);
    });
  };

  const readUploadedFileAsArrayBuffer = (inputFile) => {
    const temporaryFileReader = new FileReader();
  
    return new Promise((resolve, reject) => {
      temporaryFileReader.onerror = () => {
        temporaryFileReader.abort();
        reject(new DOMException("Problem parsing input file."));
      };
  
      temporaryFileReader.onload = () => {
        resolve(temporaryFileReader.result);
      };
      temporaryFileReader.readAsArrayBuffer(inputFile);
    });
  };

const readShapefiles = async (files) => {
  const cpgFile = files.filter(e=>e.name.split(".").pop()=="cpg");
  const cpgContents= cpgFile.length>0 ? await readUploadedFileAsText(cpgFile[0]) : "UTF-8";

  const shpFile = files.filter(e=>e.name.split(".").pop()=="shp");
  const shpContents = await readUploadedFileAsArrayBuffer(shpFile[0])  
  const dbfFile = files.filter(e=>e.name.split(".").pop()=="dbf");
  const dbfContents = await readUploadedFileAsArrayBuffer(dbfFile[0])  

  let prjFile = files.filter(e=>e.name.split(".").pop()=="prj");
  const prjContents = prjFile.length>0 ? await readUploadedFileAsText(prjFile[0]) : 'GEOGCS["WGS 84", DATUM["World Geodetic System 1984", SPHEROID["WGS 84", 6378137.0, 298.257223563, AUTHORITY["EPSG","7030"]], AUTHORITY["EPSG","6326"]], PRIMEM["Greenwich", 0.0, AUTHORITY["EPSG","8901"]], UNIT["degree", 0.017453292519943295], AXIS["Geodetic longitude", EAST], AXIS["Geodetic latitude", NORTH], AUTHORITY["EPSG","4326"]]';
  
  if(prjContents == 'GEOGCS["WGS 84", DATUM["World Geodetic System 1984", SPHEROID["WGS 84", 6378137.0, 298.257223563, AUTHORITY["EPSG","7030"]], AUTHORITY["EPSG","6326"]], PRIMEM["Greenwich", 0.0, AUTHORITY["EPSG","8901"]], UNIT["degree", 0.017453292519943295], AXIS["Geodetic longitude", EAST], AXIS["Geodetic latitude", NORTH], AUTHORITY["EPSG","4326"]]'){
    prjFile = []
  }
  
    readShape(shpContents,dbfContents,{encoding:cpgContents})
      .then(result => {
          if(prjFile.length>0){
            result.features.forEach((feature) =>{
              if(Number.isFinite(feature.geometry.coordinates[0])){
                feature.geometry.coordinates = proj4(prjContents).inverse(feature.geometry.coordinates)

              }
              if(Number.isFinite(feature.geometry.coordinates[0][0])){
                feature.geometry.coordinates = feature.geometry.coordinates.map(e=> proj4(prjContents).inverse(e))

              }
              if(Number.isFinite(feature.geometry.coordinates[0][0][0])){
                feature.geometry.coordinates = feature.geometry.coordinates.map(e=>e.map(e=> proj4(prjContents).inverse(e)))
              }
            })
          }
        const map = document.querySelector("KORTxyz-map");
              map.addGeojson(files[0].name,result)
      })

    /*
  var reader = new FileReader();

    	reader.onload = ((res) => {
        const map = document.querySelector("KORTxyz-map");
        map.addGeojson(file.name,JSON.parse(res.target.result))
   		});

    	reader.readAsText(file);
*/
}



export function load(files) {
    readShapefiles(files);
}