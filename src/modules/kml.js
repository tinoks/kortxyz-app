import {default as togeojson} from '@mapbox/togeojson';

function readKml (file) {
  var reader = new FileReader();

      reader.onload = ((res) => {
    	const dom = (new DOMParser()).parseFromString(res.target.result, 'text/xml');
        const div = document.querySelector("KORTxyz-map");
              div.addGeojson(file.name,togeojson.kml( dom ));
       
      });

      reader.readAsText(file);
}
  
export function load(file) {
    readKml(file);
}