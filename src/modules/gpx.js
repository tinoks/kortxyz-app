import {default as togeojson} from '@mapbox/togeojson';

function readGPX (file) {
  var reader = new FileReader();

      reader.onload = ((res) => {
    	const dom = (new DOMParser()).parseFromString(res.target.result, 'text/xml');
        const div = document.querySelector("KORTxyz-map");
              div.addGeojson(file.name,togeojson.gpx( dom ));
       
      });

      reader.readAsText(file);
}
  
export function load(file) {
    readGPX(file);
}