import {default as EasyFit} from 'easy-fit';

  const easyFit = new EasyFit({
    force: true,
    speedUnit: 'km/h',
    lengthUnit: 'km',
    temperatureUnit: 'celcius',
    elapsedRecordField: true,
    mode: 'cascade'
  });

function addSession(name,data){
     let geojson = {
			  "type": "FeatureCollection",
			  "features": [
			  ]
			};
      data.sessions[0].laps[0].records
        .forEach(function(e) {
          e.speed = e.speed*3.6;
          if(Math.abs(e.position_long)<90 && Math.abs(e.position_lat)<90)
          geojson.features.push(
            {'type': 'Feature',
            'properties': e,
            'geometry': {
              'type': 'Point',
              'coordinates':[e.position_long,e.position_lat]
              }
            })

          }
        );
  return geojson;
}

function readFit(file) {
  var reader = new FileReader();

    	reader.onload = ((res) => {
        easyFit.parse(res.target.result, function (error, data) {
          if (error) {
            console.err(error);
          } else {
  	        const div = document.querySelector("KORTxyz-map");
        		div.addGeojson(file.name,addSession(name,data.activity))
            
          }

        })
   		});

    	reader.readAsArrayBuffer(file);
}
  
export function load(file) {
    readFit(file);
}